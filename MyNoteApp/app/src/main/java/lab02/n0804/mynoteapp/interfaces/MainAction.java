package lab02.n0804.mynoteapp.interfaces;

import lab02.n0804.mynoteapp.model.entity.Note;

public interface MainAction {
    void getData();

    void addNote(Note note);

    void updateNote(Note note);

    void deleteNote();
}
