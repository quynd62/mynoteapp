package lab02.n0804.mynoteapp.interfaces;

import java.util.ArrayList;

import lab02.n0804.mynoteapp.model.entity.Note;

public interface MainView {
    void showProgressDialog();

    void hideProgressDialog();

    void showNoteDialog(Note note);

    void showCreateNoteActivity();

    void showUpdateNoteActivity(Note note);

    void showConfirmDialog();

    void showContent(ArrayList<Note> noteList);

    void showMessageGetDataFail();

    void showMessageAddNoteSuccess();

    void showMessageAddNoteFail();

    void showMessageUpdateNoteSuccess();

    void showMessageUpdateNoteFail();

    void showMessageDeleteNoteSuccess();

    void showMessageDeleteNoteFail();

    void notifyDataInsert();

    void notifyDataUpdate(int position);

    void notifyDataDelete(int position);
}
