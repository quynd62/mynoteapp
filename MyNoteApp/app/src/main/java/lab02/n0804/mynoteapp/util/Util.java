package lab02.n0804.mynoteapp.util;

import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import lab02.n0804.mynoteapp.App;


public class Util {
    public static String getStringById(int id){
        return App.getContext().getResources().getString(id);
    }
    public static String convertColorToHex(Drawable drawable){
        return String.format("#%06x",  (((ColorDrawable)drawable).getColor() & 0x00FFFFFF));
    }
    public static long convertStringToTime(String stringTime){
//        Date now = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy hh:mm");
        Date date = null;
        try {
            date = sdf.parse(stringTime);
        } catch (ParseException e) {
            e.printStackTrace();
        }
//        long currentTime = now.getTime();
//        long newYearDate = date.getTime();
        return date.getTime();
    }
}
