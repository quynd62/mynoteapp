package lab02.n0804.mynoteapp.view.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import org.w3c.dom.Node;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import lab02.n0804.mynoteapp.R;
import lab02.n0804.mynoteapp.adapter.NoteAdapter;
import lab02.n0804.mynoteapp.interfaces.MainView;
import lab02.n0804.mynoteapp.model.DBManager;
import lab02.n0804.mynoteapp.model.entity.Note;
import lab02.n0804.mynoteapp.presenter.MainPresenter;
import lab02.n0804.mynoteapp.services.MyAlarmReceiver;
import lab02.n0804.mynoteapp.util.Util;
import lab02.n0804.mynoteapp.view.dialog.ConfirmDialog;
import lab02.n0804.mynoteapp.view.dialog.ViewNoteDialog;

public class MainActivity extends AppCompatActivity implements MainView {

    private static final int ACTIVITY_CREATE_NOTE = 1001;
    public static final String NOTE_UPDATE = "NOTE_UPDATE";
    private static final int ACTIVITY_UPDATE_NOTE = 1002;
    private RecyclerView rcvNote;
    private ImageView btnAdd;
    private DBManager dbManager;
    private MainPresenter mainPresenter;
    private NoteAdapter noteAdapter;
    private ProgressDialog progressDialog;
    private ConfirmDialog confirmDialog;
    private ViewNoteDialog viewNoteDialog;
    public AlarmManager alarmManager ;
    public PendingIntent pendingIntent;
    long minTime = 999999999999999999L;
    EditText editTextNhapThoiGian;
    ArrayList<Note> listNote;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initViews();
        initComponents();
        initControl();
        mainPresenter.getData();
        listNote = new ArrayList<>();

        // bắt sự kiện cho editText khi text thay đổi
        editTextNhapThoiGian.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                // sau khi kết thúc thay đổi thì duyệt listNote để lấy ra những bản ghi cần và lưu vào listTimKiem
                ArrayList<Note> listTimKiem = new ArrayList<>();
                listTimKiem.clear();
                for (int i = 0; i < listNote.size();i++){
                    if (listNote.get(i).getDate().contains(s.toString())){
                        listTimKiem.add(listNote.get(i));
                    }
                }
                // sau khi lấy đc list thì phải set lại data cho adapter của recyclerview
                noteAdapter.setData(listTimKiem);
                noteAdapter.notifyDataSetChanged();
            }
        });
    }

    private void initViews() {
        rcvNote = (RecyclerView) findViewById(R.id.list_view_note);
        rcvNote.setHasFixedSize(true);
        rcvNote.setLayoutManager(new LinearLayoutManager(this));

        btnAdd = (ImageView) findViewById(R.id.btn_add);
        // edt để nhập thời gian tìm kiếm
        editTextNhapThoiGian = findViewById(R.id.edtNhapThoiGian);


    }

    private void initComponents() {
        dbManager = new DBManager();
        mainPresenter = new MainPresenter(this, dbManager);
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage(Util.getStringById(R.string.doi_teo));

        viewNoteDialog = new ViewNoteDialog(this);
        confirmDialog = new ConfirmDialog(this, mainPresenter);
        noteAdapter = new NoteAdapter(MainActivity.this, mainPresenter);
    }

    private void initControl() {
        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mainPresenter.onClickAddNote();
            }
        });
    }


    private void showToast(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showProgressDialog() {
        progressDialog.show();
    }

    @Override
    public void hideProgressDialog() {
        progressDialog.dismiss();
    }

    @Override
    public void showNoteDialog(Note note) {
        viewNoteDialog.setContent(note);
        viewNoteDialog.show();
    }

    @Override
    public void showCreateNoteActivity() {
        startActivityForResult(new Intent(this, CreateNoteActivity.class), ACTIVITY_CREATE_NOTE);
    }

    @Override
    public void showUpdateNoteActivity(Note note) {
        Intent intent = new Intent(this, CreateNoteActivity.class);
        intent.putExtra(NOTE_UPDATE, note);
        startActivityForResult(intent, ACTIVITY_UPDATE_NOTE);
    }

    @Override
    public void showConfirmDialog() {
        confirmDialog.show();
    }

    @Override
    public void showContent(ArrayList<Note> noteList) {
        listNote = noteList;
        // sắp xếp lại notelist theo thứ tự bca
        noteList.sort(new Comparator<Note>() {
            @Override
            public int compare(Note o1, Note o2) {
                if (o1.getContent().compareTo(o2.getContent()) == 0){
                    return 0;
                }
                else if(o1.getContent().compareTo(o2.getContent()) < 0){
                    return 1;
                }else return -1;
            }
        });
//        Collections.sort(noteList, new Comparator<Note>() {
//            @Override
//            public int compare(Note o1, Note o2) {
//                if (o1.getContent().compareTo(o2.getContent()) == 0){
//                    return 0;
//                }
//                else if(o1.getContent().compareTo(o2.getContent()) < 0){
//                    return 1;
//                }else return -1;
//            }
//        });
        noteAdapter.setData(noteList);
        rcvNote.setAdapter(noteAdapter);
        if (getMinNote(noteList) != null)
        alarm(getMinNote(noteList));
    }

    @Override
    public void showMessageGetDataFail() {
        showToast(Util.getStringById(R.string.tai_du_lieu_that_bai));
    }

    @Override
    public void showMessageAddNoteSuccess() {
        showToast(Util.getStringById(R.string.them_moi_thanh_cong));
    }

    @Override
    public void showMessageAddNoteFail() {
        showToast(Util.getStringById(R.string.them_moi_that_bai));
    }

    @Override
    public void showMessageUpdateNoteSuccess() {
        showToast(Util.getStringById(R.string.cap_nhat_thanh_cong));
    }

    @Override
    public void showMessageUpdateNoteFail() {
        showToast(Util.getStringById(R.string.cap_nhat_that_bai));
    }

    @Override
    public void showMessageDeleteNoteSuccess() {
        showToast(Util.getStringById(R.string.xoa_thanh_cong));
    }

    @Override
    public void showMessageDeleteNoteFail() {
        showToast(Util.getStringById(R.string.xoa_that_bai));
    }

    @Override
    public void notifyDataInsert() {
        noteAdapter.notifyDataSetChanged();
        if (getMinNote(noteAdapter.getListNoteCurrent()) != null)
        alarm(getMinNote(noteAdapter.getListNoteCurrent()));
    }

    @Override
    public void notifyDataUpdate(int position) {
        noteAdapter.notifyItemChanged(position);
        if (getMinNote(noteAdapter.getListNoteCurrent()) != null)
        alarm(getMinNote(noteAdapter.getListNoteCurrent()));
    }

    @Override
    public void notifyDataDelete(int position) {
        noteAdapter.notifyItemRemoved(position);
        if (getMinNote(noteAdapter.getListNoteCurrent()) != null)
        alarm(getMinNote(noteAdapter.getListNoteCurrent()));
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == ACTIVITY_CREATE_NOTE) {
            if (resultCode == CreateNoteActivity.RESULT_OK) {
                Note note = (Note) data.getSerializableExtra(CreateNoteActivity.NOTE_DATA);
                mainPresenter.addNote(note);
            }
        }
        if (requestCode == ACTIVITY_UPDATE_NOTE) {
            Note note = (Note) data.getSerializableExtra(CreateNoteActivity.NOTE_DATA);
            mainPresenter.updateNote(note);
        }
        super.onActivityResult(requestCode, resultCode, data);
    }
    //hàm để chạy ngầm báo thức cho 1 ghi chú
    public void alarm(Note note){
//        if (alarmManager != null) {
//            alarmManager.cancel(new AlarmManager.OnAlarmListener() {
//                @Override
//                public void onAlarm() {
//
//                }
//            });
//        }
        if (pendingIntent != null){
            alarmManager.cancel(pendingIntent);
            pendingIntent.cancel();
        }
        alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);
        Intent intentAlarm = new Intent(MainActivity.this, MyAlarmReceiver.class);
        intentAlarm.setAction("ALARM_NOTE");
//        intentAlarm.putExtra("note",note);
//        intentAlarm.putExtra("THONG_BAO","Sao khong chuyen duoc object sang broadcast");
        Bundle bundle = new Bundle();
//        bundle.putParcelable("note",note);
        bundle.putString("THONG_BAO",note.getContent());
        intentAlarm.putExtras(bundle);
        pendingIntent = PendingIntent.getBroadcast(MainActivity.this,5,intentAlarm,PendingIntent.FLAG_UPDATE_CURRENT);
        // tim thoi gian cua node gan nhat roi tien hanh truyen vao itentAlarm để notify hoặc đọc nội dung node để thông báo đến người dùng
        alarmManager.set(AlarmManager.RTC_WAKEUP,Util.convertStringToTime(note.getDate()),pendingIntent);
//        sendBroadcast(intentAlarm);
    }
    /**
     *Hàm tìm Note có thời gian lớn hơn gần nhất so với thời điểm hiện tại
     */
    public Note getMinNote(List<Note> list){
        Note minNote = new Note();
        if (list.size() > 0){
            for (int i = 0 ; i < list.size() ;i++){
                if (Util.convertStringToTime(list.get(i).getDate()) > System.currentTimeMillis() && Util.convertStringToTime(list.get(i).getDate()) <= minTime ){
                    minTime = Util.convertStringToTime(list.get(i).getDate());
                    minNote = list.get(i);
                }
            }
            if (minNote.getColor() == null) return null;
        }else{
            return null;
        }
        return minNote;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
//        if (alarmManager != null) {
//            alarmManager.cancel(new AlarmManager.OnAlarmListener() {
//                @Override
//                public void onAlarm() {
//
//                }
//            });
//        }
    }
}