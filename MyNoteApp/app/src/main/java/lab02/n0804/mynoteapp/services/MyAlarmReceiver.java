package lab02.n0804.mynoteapp.services;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

import lab02.n0804.mynoteapp.model.entity.Note;

public class MyAlarmReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        Toast.makeText(context, "thong bao cho node", Toast.LENGTH_SHORT).show();

//        Note note = (Note) intent.getExtras().getParcelable("note");
        String a = intent.getExtras().getString("THONG_BAO");
        Log.e("kevin", "onReceive: " + a );
        // Tạo intent truyền dữ liệu qua cho service xử lý
        Intent serviceIntent = new Intent(context, TextToSpeechSerVice.class);
        serviceIntent.putExtra("noteMin", a);
        context.startService(serviceIntent);

    }
}
