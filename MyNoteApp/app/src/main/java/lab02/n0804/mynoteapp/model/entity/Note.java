package lab02.n0804.mynoteapp.model.entity;

import android.net.Uri;

import java.io.Serializable;

import lab02.n0804.mynoteapp.R;

public class Note implements Serializable {

    private int id;
    private String date;
    private String content;
    private String color;
    private String uri;


    public Note(int id, String date, String content, String color,String uri) {
        this.id = id;
        this.date = date;
        this.content = content;
        this.color = color;
        this.uri = uri;
    }

    public Note() {

    }

    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }
}
