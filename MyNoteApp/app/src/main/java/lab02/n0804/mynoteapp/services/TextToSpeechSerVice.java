package lab02.n0804.mynoteapp.services;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.IBinder;
import android.speech.tts.TextToSpeech;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;

import java.util.Locale;

import lab02.n0804.mynoteapp.R;
import lab02.n0804.mynoteapp.model.entity.Note;
import lab02.n0804.mynoteapp.view.activity.MainActivity;

public class TextToSpeechSerVice extends Service {
    TextToSpeech textToSpeech;
    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.e("david","Service duoc goi den");
        String note = intent.getStringExtra("noteMin");
        Toast.makeText(this, "text to speech service đang được gọi tới", Toast.LENGTH_SHORT).show();
//        textToSpeech = new TextToSpeech(getApplicationContext(), new TextToSpeech.OnInitListener() {
//            @Override
//            public void onInit(int status) {
//                textToSpeech.setLanguage(Locale.getDefault());
//            }
//        });
//        textToSpeech.speak("Mình đang sử dụng Text to Speech của google để chuyển đổi",<></>extToSpeech.QUEUE_FLUSH,null);


        createNotificationChannel();
        NotificationCompat.Builder builder =
                new NotificationCompat.Builder(this,"10")
                        .setSmallIcon(R.drawable.ic_speech_to_text)
                        .setContentTitle("My Note App")
                        .setContentText(note)
                        .setAutoCancel(true)
                .setStyle(new NotificationCompat.BigTextStyle().bigText(note))
                ;

        Intent notificationIntent = new Intent(this, TextToSpeechSerVice.class);
        PendingIntent contentIntent = PendingIntent.getService(this, 9, notificationIntent,
                PendingIntent.FLAG_UPDATE_CURRENT);
        builder.setContentIntent(contentIntent);

        // Add as notification
        NotificationManagerCompat manager = NotificationManagerCompat.from(this);
        manager.notify(0, builder.build());
        return START_NOT_STICKY;
    }

    @Override
    public void onDestroy() {
//        textToSpeech.stop();
//        textToSpeech.shutdown();
        super.onDestroy();
    }

    private void createNotificationChannel() {
        // Create the NotificationChannel, but only on API 26+ because
        // the NotificationChannel class is new and not in the support library
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = "getString(R.string.channel_name)";
            String description = "getString(R.string.channel_description)";
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            NotificationChannel channel = new NotificationChannel("10", name, importance);
            channel.setDescription(description);
            // Register the channel with the system; you can't change the importance
            // or other notification behaviors after this
            NotificationManager notificationManager = getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(channel);
        }
    }

    @Override
    public void onCreate() {
        super.onCreate();
    }

}
