package lab02.n0804.mynoteapp.adapter;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import lab02.n0804.mynoteapp.R;
import lab02.n0804.mynoteapp.model.entity.Note;
import lab02.n0804.mynoteapp.presenter.MainPresenter;
import lab02.n0804.mynoteapp.services.MyAlarmReceiver;
import lab02.n0804.mynoteapp.services.TextToSpeechSerVice;
import lab02.n0804.mynoteapp.view.activity.MainActivity;

import static android.content.Context.ALARM_SERVICE;

public class NoteAdapter extends RecyclerView.Adapter<NoteAdapter.MyViewHolder> {
    private Activity context;
    private ArrayList<Note> noteList = new ArrayList<>();
    private LayoutInflater inflater;
    private MainPresenter mainPresenter;

    public NoteAdapter(Activity context, MainPresenter mainPresenter) {
        this.context = context;
        this.mainPresenter = mainPresenter;
        inflater = LayoutInflater.from(context);
    }

    public void setData(ArrayList<Note> noteList) {
        this.noteList = noteList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.item_note, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        holder.lnItem.setBackgroundColor(Color.parseColor(noteList.get(position).getColor()));
        holder.date.setText(noteList.get(position).getDate());
        holder.content.setText(noteList.get(position).getContent());
        holder.date.setText(noteList.get(position).getDate());
        if (noteList.get(position).getUri() == null){
            Toast.makeText(context, "khong co uri cua anh", Toast.LENGTH_SHORT).show();
        }else {
            if (!noteList.get(position).getUri().equals("abc")){
                Picasso.with(context).load(Uri.parse(noteList.get(position).getUri())).into(holder.imvAvatar);
            }
        }
//        holder.imvAvatar.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent serviceIntent = new Intent(context, TextToSpeechSerVice.class);
//                context.startService(serviceIntent);
//            }
//        });
//        holder.imageViewBaoThuc.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                // Sử lý để tiến hành sử dụng alarmManager để bắn pending itent sang cho broadcast
//            }
//        });
    }

    @Override
    public int getItemCount() {
        if (noteList != null) {
            return noteList.size();
        }
        return 0;
    }

    protected class MyViewHolder extends RecyclerView.ViewHolder {
        private LinearLayout lnItem;
        private TextView date;
        private TextView content;
        private ImageView delete;
        private ImageView edit;
        private CircleImageView imvAvatar;
        private ImageView imageViewBaoThuc;

        public MyViewHolder(View itemView) {
            super(itemView);
            lnItem = (LinearLayout) itemView.findViewById(R.id.ln_item);
            date = (TextView) itemView.findViewById(R.id.txt_date);
            content = (TextView) itemView.findViewById(R.id.tv_content);
            delete = (ImageView) itemView.findViewById(R.id.btn_delete);
            edit = (ImageView) itemView.findViewById(R.id.btn_edit);
            imvAvatar = (CircleImageView) itemView.findViewById(R.id.imv_avartar_note);
            imageViewBaoThuc = (ImageView) itemView.findViewById(R.id.imvBaoThuc);

            lnItem.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mainPresenter.onClickNoteAt(getAdapterPosition());
                }
            });
            delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mainPresenter.onDeleteNoteAt(getAdapterPosition());
                }
            });
            edit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mainPresenter.onUpdateNoteAt(getAdapterPosition());
                }
            });
        }
    }
    //hàm để chạy ngầm báo thức cho 1 ghi chú
    public void alarm(Note note){
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(ALARM_SERVICE);
        Intent intentAlarm = new Intent(context, MyAlarmReceiver.class);
        intentAlarm.putExtra("note",note);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context,5,intentAlarm,PendingIntent.FLAG_UPDATE_CURRENT);
        // tim thoi gian cua node gan nhat roi tien hanh truyen vao itentAlarm để notify hoặc đọc nội dung node để thông báo đến người dùng
        alarmManager.set(AlarmManager.RTC_WAKEUP,System.currentTimeMillis()+10000,pendingIntent);
    }

    public List<Note> getListNoteCurrent(){
        return noteList;
    }
}
