package lab02.n0804.mynoteapp.view.activity;

import android.app.Activity;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.speech.RecognizerIntent;
import android.text.InputType;
import android.text.TextUtils;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TimePicker;
import android.widget.Toast;

import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Locale;

import lab02.n0804.mynoteapp.R;
import lab02.n0804.mynoteapp.model.entity.Note;
import lab02.n0804.mynoteapp.util.Util;

import static lab02.n0804.mynoteapp.App.getContext;

public class CreateNoteActivity extends Activity implements View.OnClickListener {
    public static final int RESULT_OK = 1;
    public static final int RESULT_NULL = 0;
    public static final String NOTE_DATA = "NOTE_DATA";

    private EditText edtContenNote;
    private DatePicker datePicker;
    private View[] colorPick;
    private Button btnCreateNote;
    private HashMap<Integer, String> hashMapStringHexColor;
    private int[] ids;
    private Note note;
    private ImageView imvAnhMinhHoa;
    private ImageView imvSpeechToText;
    private Uri imageUri = null;
    private EditText editTextNhapThoiGian;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.creat_note_activity);
        note = (Note) getIntent().getSerializableExtra(MainActivity.NOTE_UPDATE);

        findViews();
        initComponents();
        setEvents();
    }

    private void initComponents() {

        ids = new int[]{R.id.note_1, R.id.note_2, R.id.note_3, R.id.note_4, R.id.note_5};

        hashMapStringHexColor = new HashMap<>();
        hashMapStringHexColor.put(ids[0], Util.getStringById(R.color.colorPick1));
        hashMapStringHexColor.put(ids[1], Util.getStringById(R.color.colorPick2));
        hashMapStringHexColor.put(ids[2], Util.getStringById(R.color.colorPick3));
        hashMapStringHexColor.put(ids[3], Util.getStringById(R.color.colorPick4));
        hashMapStringHexColor.put(ids[4], Util.getStringById(R.color.colorPick5));

        colorPick = new View[hashMapStringHexColor.size()];
        int i = 0;
        for (View view : colorPick) {
            view = findViewById(getIDColorPick(i));
            i++;
            view.setOnClickListener(this);
        }
    }

    private void setEvents() {
        if (note != null) {
            btnCreateNote.setText(Util.getStringById(R.string.cap_nhat));
            edtContenNote.setText(note.getContent());
            edtContenNote.setBackgroundColor(Color.parseColor(note.getColor()));

            // Phân tích lại chuỗi để lấy ra thời gian
            String[] date = note.getDate().split(" ");
            String[] date1 = date[0].split("/");

            datePicker.init(Integer.parseInt(date1[2]), (Integer.parseInt(date1[1]) - 1), Integer.parseInt(date1[0]), null);
            editTextNhapThoiGian.setText(date[1]);
        }

        btnCreateNote.setOnClickListener(this);
        datePicker.requestFocus();
        edtContenNote.setMovementMethod(new ScrollingMovementMethod());
        imvAnhMinhHoa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent choosePicture = new Intent();
                choosePicture.setType("image/*");
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                    choosePicture.setAction(Intent.ACTION_OPEN_DOCUMENT);
                } else {
                    choosePicture.setAction(Intent.ACTION_GET_CONTENT);
                }
                startActivityForResult(choosePicture, 1);
            }
        });
        imvSpeechToText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentSpeech = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
                intentSpeech.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
                intentSpeech.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault());
                intentSpeech.putExtra(RecognizerIntent.EXTRA_PROMPT, "Bạn hãy nói gì đó");
                try {
                    startActivityForResult(intentSpeech, 2);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        editTextNhapThoiGian.setInputType(InputType.TYPE_NULL);
        editTextNhapThoiGian.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar calendar = Calendar.getInstance();
                int hour = calendar.get(Calendar.HOUR_OF_DAY);
                int minute = calendar.get(Calendar.MINUTE);
                TimePickerDialog timePickerDialog= new TimePickerDialog(CreateNoteActivity.this, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        editTextNhapThoiGian.setText(hourOfDay + ":" + minute);

                    }
                },hour,minute,true);
                timePickerDialog.show();

            }
        });
    }

    private int getIDColorPick(int i) {
        return ids[i];
    }

    private void findViews() {
        edtContenNote = findViewById(R.id.edt_conten_note);
        datePicker = findViewById(R.id.date_picker);
        btnCreateNote = findViewById(R.id.btn_create_note);
        imvAnhMinhHoa = findViewById(R.id.imv_anhminhhoa);
        imvSpeechToText = findViewById(R.id.imv_speech_to_text);
        editTextNhapThoiGian = findViewById(R.id.edtTimepick);

    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.btn_create_note) {
            if (TextUtils.isEmpty(edtContenNote.getText())) {
                edtContenNote.setError(Util.getStringById(R.string.nhap_ghi_nho));
                edtContenNote.requestFocus();
                return;
            }

            Note note = new Note();
            note.setContent(edtContenNote.getText().toString().trim());
            if (editTextNhapThoiGian.getText().toString() == null || editTextNhapThoiGian.getText().toString().equals("")){
                Toast.makeText(this, "Bạn phải chọn thời gian!!!", Toast.LENGTH_SHORT).show();
            }else {
                note.setDate(datePicker.getDayOfMonth() + "/" + (datePicker.getMonth() + 1) + "/" + datePicker.getYear() + " " + editTextNhapThoiGian.getText().toString());
            }
            note.setColor(Util.convertColorToHex(edtContenNote.getBackground()));
            if (imageUri != null){
                note.setUri(imageUri.toString());
            }else {
                Toast.makeText(this, "null uri", Toast.LENGTH_SHORT).show();
            }

            if (editTextNhapThoiGian.getText().toString() == null || editTextNhapThoiGian.getText().toString().equals("")){
                Toast.makeText(this, "Bạn phải chọn thời gian!!!", Toast.LENGTH_SHORT).show();
            }else {
                Intent intent = new Intent();

                intent.putExtra(NOTE_DATA, note);

                setResult(RESULT_OK, intent);
                finish();
            }

        } else {
            if (!v.isSelected()) {
                setColorView(v.getId());
            }
        }
    }

    @Override
    public void onBackPressed() {
        setResult(RESULT_NULL);
        super.onBackPressed();
    }

    private void setColorView(int id) {
        edtContenNote.setBackgroundColor(Color.parseColor(hashMapStringHexColor.get(id)));

    }
    
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case 1:{
                if (resultCode == -1 && null != data) {
                    imageUri = data.getData();

                    try{
                        Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(),imageUri);
                        imvAnhMinhHoa.setImageBitmap(bitmap);
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                    imvAnhMinhHoa.setImageURI(imageUri);
                }
                break;
            }
            case 2: {
                if (resultCode == -1 && null != data) {

                    ArrayList<String> result = data
                            .getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
                    edtContenNote.setText(result.get(0));
                }
                break;
            }

        }
    }
}
