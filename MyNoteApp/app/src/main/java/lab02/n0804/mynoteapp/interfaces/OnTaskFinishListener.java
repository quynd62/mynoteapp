package lab02.n0804.mynoteapp.interfaces;

import java.util.ArrayList;

import lab02.n0804.mynoteapp.model.entity.Note;

public interface OnTaskFinishListener {
    void onStartTask();

    void onFinishTask();

    void onGetDataSuccess(ArrayList<Note> noteList);

    void onGetDataFail();

    void onAddNoteSuccess();

    void onAddNoteFail();

    void onDeleteNoteSuccess();

    void onDeleteNoteFail();

    void onUpdateNoteSuccess();

    void onUpdateNoteFail();
}
